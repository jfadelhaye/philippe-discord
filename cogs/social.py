from discord.ext import commands


class Social(commands.Cog):
    """
    Module d'interaction avec les réseaux sociaux
    """
    def __init__(self, client):
        self.client = client

    @commands.command()
    async def insta(self, ctx, username):
        """
        Retourne le lien insta de quelqu'un
        """
        await ctx.send("https://instagram.com/{}".format(username))


def setup(client):
    client.add_cog(Social(client))
