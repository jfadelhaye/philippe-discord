import discord
from discord.ext import commands
from discord.errors import Forbidden

from itertools import accumulate
from bisect import bisect
from random import randrange

# thanks https://gist.github.com/shello/efa2655e8a7bce52f273 !

UNICODE_VERSION = 6
EMOJI_RANGES_UNICODE = {
    6: [
        ('\U0001F300', '\U0001F320'),
        ('\U0001F330', '\U0001F335'),
        ('\U0001F337', '\U0001F37C'),
        ('\U0001F380', '\U0001F393'),
        ('\U0001F3A0', '\U0001F3C4'),
        ('\U0001F3C6', '\U0001F3CA'),
        ('\U0001F3E0', '\U0001F3F0'),
        ('\U0001F400', '\U0001F43E'),
        ('\U0001F440', ),
        ('\U0001F442', '\U0001F4F7'),
        ('\U0001F4F9', '\U0001F4FC'),
        ('\U0001F500', '\U0001F53C'),
        ('\U0001F540', '\U0001F543'),
        ('\U0001F550', '\U0001F567'),
        ('\U0001F5FB', '\U0001F5FF')
    ],
    7: [
        ('\U0001F300', '\U0001F32C'),
        ('\U0001F330', '\U0001F37D'),
        ('\U0001F380', '\U0001F3CE'),
        ('\U0001F3D4', '\U0001F3F7'),
        ('\U0001F400', '\U0001F4FE'),
        ('\U0001F500', '\U0001F54A'),
        ('\U0001F550', '\U0001F579'),
        ('\U0001F57B', '\U0001F5A3'),
        ('\U0001F5A5', '\U0001F5FF')
    ],
    8: [
        ('\U0001F300', '\U0001F579'),
        ('\U0001F57B', '\U0001F5A3'),
        ('\U0001F5A5', '\U0001F5FF')
    ]
}

NO_NAME_ERROR = '(No name found for this codepoint)'


async def send_embed(ctx, embed):
    """
    Function that handles the sending of embeds
    -> Takes context and embed to send
    - tries to send embed in channel
    - tries to send normal message when that fails
    - tries to send embed private with information abot missing permissions
    If this all fails: https://youtu.be/dQw4w9WgXcQ
    """
    try:
        return await ctx.send(embed=embed)
    except Forbidden:
        try:
            await ctx.send("Hey, seems like I can't send embeds. Please check my permissions :)")
        except Forbidden:
            await ctx.author.send(
                f"Hey, seems like I can't send any message in {ctx.channel.name} on {ctx.guild.name}\n"
                f"May you inform the server team about this issue? :slight_smile: ", embed=embed)


def random_emoji(unicode_version=6):
    if unicode_version in EMOJI_RANGES_UNICODE:
        emoji_ranges = EMOJI_RANGES_UNICODE[unicode_version]
    else:
        emoji_ranges = EMOJI_RANGES_UNICODE[-1]

    # Weighted distribution
    count = [ord(r[-1]) - ord(r[0]) + 1 for r in emoji_ranges]
    weight_distr = list(accumulate(count))

    # Get one point in the multiple ranges
    point = randrange(weight_distr[-1])

    # Select the correct range
    emoji_range_idx = bisect(weight_distr, point)
    emoji_range = emoji_ranges[emoji_range_idx]

    # Calculate the index in the selected range
    point_in_range = point
    if emoji_range_idx != 0:
        point_in_range = point - weight_distr[emoji_range_idx - 1]

    # Emoji
    emoji = chr(ord(emoji_range[0]) + point_in_range)

    return emoji


class Interact(commands.Cog):
    """
    Module d'interaction entre les membres ( sondages, jeux, etc ...)
    """
    def __init__(self, client):
        self.client = client

    @commands.command()
    async def sondage(self, ctx, question, *answers):
        """
        Créé un sondage
        """
        if len(answers) > 20:
            await ctx.send("désolé, il n'est pas possible d'avoir plus de 20 choix !")
        else:
            association = {}
            reactions = []
            if len(answers) == 1:
                # only one reaction
                reactions = ['👍']
            elif len(answers) == 2:
                reactions = ['👍', '👎']
            elif len(answers) > 2:
                for i in range(0, len(answers)):
                    reactions.append(random_emoji())

            for i, a in enumerate(answers):
                association[a] = reactions[i]

            emb = discord.Embed(title='Sondage', color=discord.Color.blue())
            emb.add_field(name='Question', value=question, inline=False)
            reponses = ""
            for k, v in association.items():
                reponses += "- {}: {}\n".format(v, k)

            emb.add_field(name='Réponse', value=reponses, inline=False)
            sent = await send_embed(ctx, emb)

            for emoji in reactions:
                await sent.add_reaction(emoji)


def setup(client):
    client.add_cog(Interact(client))
