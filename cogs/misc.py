import random
from discord.ext import commands


class Misc(commands.Cog):
    """
    Commandes diverses
    """
    def __init__(self, client):
        self.client = client

    @commands.command()
    async def choose(self, ctx, *choices: str):
        """
        Choisis entre différentes posibilitées
        """
        await ctx.send(random.choice(choices))

    @commands.command()
    async def roll(self, ctx, dice: str):
        """Lance les dés ( ex : 3d100 )"""
        try:
            rolls, limit = map(int, dice.split('d'))
        except Exception:
            await ctx.send('Format has to be in NdN!')
            return

        result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
        await ctx.send(result)

    @commands.command(name='avis')
    async def avis(self, ctx):
        """
        Donne une réponse aléatoire
        """
        answers = [
            "Essaye plus tard.",
            "Essaye encore.",
            "Pas d'avis.",
            "C'est ton destin.",
            "Le sort en est jeté.",
            "Une chance sur deux.",
            "Repose ta question.",
            "D'après moi, oui.",
            "C'est certain.",
            "Oui, Absolument !",
            "Tu peux compter la dessus !",
            "Sans aucun doute.",
            "Très probable.",
            "Oui",
            "C'est bien parti",
            "C'est non.",
            "Peu probable.",
            "Faut pas réver.",
            "N'y comptes pas.",
            "Impossible.",
            "Et la marmotte, elle mets le chocolat dans le papier d'alu ..."
            "Réfléchis-y longuement ..."]
        await ctx.send(random.choice(answers))


def setup(client):
    client.add_cog(Misc(client))
