Phlippe is my compagnion bot for discord

# installation 

First thing first : you need to [register a bot on discord][1]

Then, copy this repo, and install every dependencies

```
git clone https://gitlab.com/jfadelhayetest/philippe-discord
cd philippe-discord
python3.7 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

# usage 
philippe needs his DISCORD_TOKEN to be able to connect, you
must provide it either by passing it as an environment variable or 
by creating a `.env` file, where you'll store it   

```
DISCORD_TOKEN=<your bots token>
PREFIX=<command prefix>
VERSION=<version number>
OWNER_NAME=<your username#1212>
OWNER_ID=<your account ID>
```

When the env vars are set, you can just run `python philippe.py`
and you are ready to go ! 

# Documentation 

Philippe is build on the great discord.py lib ! 
Checkout its [documentation][1] to modify it ( and give it another name please, 
there is one and only one philippe ).

# Compatibility 

Should virtually run on anything capable of running python. 

Tested flawlessly on ubuntu, archlinux, FreeBSD,
FreeBSD-arm64 and NetBSD-armv6.

[1]: https://discordpy.readthedocs.io/en/stable/discord.html
